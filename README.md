------------
⚠️**THIS REPOSITORY HAS BEEN MIGRATED TO GitHub!**⚠️

This repository is **closed and archived** in favor of the GitHub repository, where development will continue.

Repository has been moved to a new place here:

* https://github.com/libgme/game-music-emu

**See details:**

* [Issue 45 (Chances of moving to Github)](https://bitbucket.org/mpyne/game-music-emu/issues/45)
* [Pull-Request 40 (Preparation for moving to GitHub, updating info)](https://bitbucket.org/mpyne/game-music-emu/pull-requests/40)
------------
